const path = require('path');
const webpack = require('webpack');
const filePath = require('./path-webpack.json')

 module.exports = {
    mode: 'development',
     entry: '../src/index.html',
     output: {
         path: path.resolve(__dirname, '../public'),
         filename: 'index.html',
         publicPath: "/assets/"
     },
     module: {
         rules: [
             {
                test: /\.js$/,
                include: [
                    path.resolve(__dirname, "../src/js")],
                exclude: [
                    path.resolve(__dirname, "../src/sass","../src/img")],
                 loader: 'babel-loader',
                 query: {
                     presets: ['babel-preset-env']
                 }
             }
         ]
     },
     stats: {
         colors: true
     },
     devtool: 'source-map'
 };